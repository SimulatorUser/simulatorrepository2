#Demo

FROM microsoft/dotnet:sdk AS build-env
WORKDIR /app
EXPOSE 80

COPY *.csproj ./
RUN dotnet restore 

COPY . ./
RUN dotnet publish -c Release -o out

FROM microsoft/dotnet:aspnetcore-runtime
WORKDIR /app
COPY --from=build-env /app/out .
ENTRYPOINT ["dotnet","SimulatorDemo.dll"]